#!/usr/bin/env bash

for ((i = 0; i < $(nproc); i++)); do cpufreq-set -c $i -g performance; done
sysctl kernel.kptr_restrict=0
sysctl kernel.perf_event_paranoid=-1