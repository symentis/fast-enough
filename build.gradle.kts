import com.github.jrubygradle.api.core.RepositoryHandlerExtension
import java.io.FileNotFoundException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption

plugins {
    id("org.asciidoctor.jvm.revealjs") version "3.3.2"
    id("org.asciidoctor.jvm.gems") version "3.3.2"
    id("org.kordamp.gradle.livereload") version "0.4.0"
}

repositories {
    gradlePluginPortal()
    // https://github.com/jruby-gradle/jruby-gradle-plugin/issues/407
    this as ExtensionAware
    the<RepositoryHandlerExtension>().gems()
}

dependencies {
    dependencies {
        asciidoctorGems("rubygems:asciidoctor-revealjs:4.1.0")
    }
}

tasks.asciidoctorRevealJs {
    sourceDir("src/main/slides")
    sources {
        include("index.adoc")
    }
    setOutputDir("build/slides")
    resources {
        from("src/main/resources") {
            include("**")
        }
    }
    asciidoctorj {
        modules {
            diagram.setVersion("2.2.1")
        }
    }
}

tasks.liveReload {
    setDocRoot(tasks.asciidoctorRevealJs.get().outputDir.absolutePath)
}

tasks.register("buildWorkshop") {
    doLast {
        exec {
            workingDir("jvmperformance-workshop")
            executable("./mvnw")
            args("-B", "clean", "verify")
        }
    }
}

tasks.register("getMeasurments") {
    doLast {
        val measurmentsDir = project.buildDir.toPath().resolve("measurments")
        Files.createDirectories(measurmentsDir)

        val asyncProfilerConfigurationTemplate =
            "async:libPath=%s;dir=%s;output=flamegraph"

        val jfrProfilerConfigurationTemplate = "jfr:dir=%s";

        val asyncProfilerLib =
            Paths.get(System.getenv("ASYNC_PROFILER_DIR")).resolve("build/libasyncProfiler.so");
        if (Files.notExists(asyncProfilerLib)) {
            throw FileNotFoundException("missing async profiler lib $asyncProfilerLib")
        }

        val stopwordsClasses = listOf(
            "pl.symentis.wordcount.core.NonThreadLocalStopwords",
            "pl.symentis.wordcount.stopwords.ThreadLocalStopwords",
            "pl.symentis.wordcount.stopwords.ICUThreadLocalStopwords"
        )

        val mapperOutputClasses = listOf(
            "pl.symentis.mapreduce.core.HashMapOutput",
            "pl.symentis.mapreduce.mapper.EclipseCollectionsMultiMapOutput",
            "pl.symentis.mapreduce.mapper.GuavaMultiMapOutput"
        )

        val threadPoolSizes = listOf(4, 8, 16, 32)

        // measurement runs for stopwords class impl
        exec {
            workingDir("jvmperformance-workshop")
            executable("java")
            args(
                jmhOptionsBuilder("sequential.SequentialMapReduceWordCountBenchmark")
                    .parameter("stopwordsClass", stopwordsClasses)
                    .parameter("mapperOutputClass", "pl.symentis.mapreduce.core.HashMapOutput")
                    .results(measurmentsDir.resolve("SequentialMapReduceWordCountBenchmark-stopwordsClass.csv"))
                    .build()
            )
        }

        // async profiling runs for stopwords impls
        stopwordsClasses.forEach {
            val outputDir =
                Files.createTempDirectory(Paths.get("build"), "async-profiler");
            exec {
                // rewrite env variable
                environment("ASYNC_PROFILER_DIR", "${System.getenv("ASYNC_PROFILER_DIR")}")
                workingDir("jvmperformance-workshop")
                executable("java")
                args(
                    jmhOptionsBuilder("sequential.SequentialMapReduceWordCountBenchmark")
                        .parameter("stopwordsClass", it)
                        .parameter("mapperOutputClass", "pl.symentis.mapreduce.core.HashMapOutput")
                        .profiler(
                            asyncProfilerConfigurationTemplate.format(
                                asyncProfilerLib.toAbsolutePath(),
                                outputDir.toAbsolutePath()
                            )
                        )
                        .build()
                )
            }

            copyProfilerRecording(
                outputDir.toAbsolutePath(),
                "flame-cpu-forward.svg",
                measurmentsDir
                    .resolve("SequentialMapReduceWordCountBenchmark-stopwordsClass-${it.substringAfterLast(".")}.svg")
            )

        }

        // measurement runs for mapper output class impl
        exec {
            workingDir("jvmperformance-workshop")
            executable("java")
            args(
                jmhOptionsBuilder("sequential.SequentialMapReduceWordCountBenchmark")
                    .parameter("stopwordsClass", "pl.symentis.wordcount.stopwords.ICUThreadLocalStopwords")
                    .parameter("mapperOutputClass", mapperOutputClasses)
                    .results(measurmentsDir.resolve("SequentialMapReduceWordCountBenchmark-mapperOutputClass.csv"))
                    .build()
            )
        }

        // async profiling runs for mapper output impls
        mapperOutputClasses.forEach {
            val outputDir =
                Files.createTempDirectory(Paths.get("build"), "async-profiler");
            exec {
                // rewrite env variable
                environment("ASYNC_PROFILER_DIR", "${System.getenv("ASYNC_PROFILER_DIR")}")
                workingDir("jvmperformance-workshop")
                executable("java")
                args(
                    jmhOptionsBuilder("sequential.SequentialMapReduceWordCountBenchmark")
                        .parameter("stopwordsClass", "pl.symentis.wordcount.stopwords.ICUThreadLocalStopwords")
                        .parameter("mapperOutputClass", it)
                        .profiler(
                            asyncProfilerConfigurationTemplate.format(
                                asyncProfilerLib.toAbsolutePath(),
                                outputDir.toAbsolutePath()
                            )
                        )
                        .build()
                )
            }

            copyProfilerRecording(
                outputDir.toAbsolutePath(),
                "flame-cpu-forward.svg",
                measurmentsDir
                    .resolve("SequentialMapReduceWordCountBenchmark-mapperOutputClass-${it.substringAfterLast(".")}.svg")
            )
        }

        // parallel measurement runs for different threads pool sizes
        exec {
            workingDir("jvmperformance-workshop")
            executable("java")
            args(
                jmhOptionsBuilder("parallel.ParallelMapReduceWordCountBenchmark")
                    .parameter("threadPoolMaxSize", threadPoolSizes.map { it.toString() })
                    .results(measurmentsDir.resolve("ParallelMapReduceWordCountBenchmark-threadPoolSize.csv"))
                    .build()
            )
        }

        // JFR profiling run for different thread pool sizes
        threadPoolSizes.forEach {
            val outputDir =
                Files.createTempDirectory(Paths.get("build"), "jfr")
            exec {
                // rewrite env variable
                environment("ASYNC_PROFILER_DIR", "${System.getenv("ASYNC_PROFILER_DIR")}")
                workingDir("jvmperformance-workshop")
                executable("java")
                args(
                    jmhOptionsBuilder("parallel.ParallelMapReduceWordCountBenchmark")
                        .parameter("threadPoolMaxSize", it.toString())
                        .profiler(jfrProfilerConfigurationTemplate.format(outputDir.toAbsolutePath()))
                        .build()
                )
            }

            copyProfilerRecording(
                outputDir.toAbsolutePath(),
                "profile.jfr",
                measurmentsDir.resolve("ParallelMapReduceWordCountBenchmark-threadPoolSize-$it.jfr")
            )
        }

        // batching measurement runs for different threads pool sizes
        exec {
            workingDir("jvmperformance-workshop")
            executable("java")
            args(
                jmhOptionsBuilder("batching.BatchingParallelMapReduceWordCountBenchmark")
                    .parameter("threadPoolMaxSize", threadPoolSizes.map { it.toString() })
                    .results(measurmentsDir.resolve("BatchingParallelMapReduceWordCountBenchmark-threadPoolSize.csv"))
                    .build()
            )
        }

        // fork/join  measurement runs
        exec {
            workingDir("jvmperformance-workshop")
            executable("java")
            args(
                jmhOptionsBuilder("forkjoin.ForkJoinMapReduceWordCountBenchmark")
                    .results(measurmentsDir.resolve("ForkJoinMapReduceWordCountBenchmark.csv"))
                    .build()
            )
        }

        // measurements for all benchmarks
        exec {
            workingDir("jvmperformance-workshop")
            executable("java")
            args(
                jmhOptionsBuilder("")
                    .results(measurmentsDir.resolve("all-benchmarks.csv"))
                    .build()
            )
        }
    }
}

class JmhOptionsBuilder(private val benchmark: String) {

    private val parameters: MutableMap<String, String> = mutableMapOf()
    private var results: Path? = null
    private var profiler: String? = null

    fun parameter(name: String, values: List<String>): JmhOptionsBuilder {
        parameters.put(name, values.joinToString(","))
        return this
    }

    fun results(path: Path): JmhOptionsBuilder {
        results = path
        return this
    }

    fun profiler(profiler: String): JmhOptionsBuilder {
        this.profiler = profiler
        return this
    }

    fun build(): List<String> {
        val args = mutableListOf<String>()
        args += listOf("-jar", "mapreduce-perf/target/benchmarks.jar")
        if (benchmark.isNotBlank()) {
            args += benchmark
        }
        args += listOf("-f", "1")

        parameters.forEach { t, u -> args.addAll(listOf("-p", "$t=$u")) }

        if (profiler != null) {
            args += listOf("-prof", profiler!!)
        }

        if (results != null) {
            args += listOf("-rff", results.toString())
        }

        return args
    }

    fun parameter(name: String, value: String): JmhOptionsBuilder {
        parameters.put(name, value)
        return this
    }
}

fun jmhOptionsBuilder(benchmark: String): JmhOptionsBuilder {
    return JmhOptionsBuilder(benchmark)
}

fun copyProfilerRecording(outputDir: Path, recordingFilename: String, destPath: Path) {
    val recording = Files.find(outputDir.toAbsolutePath(),
        3,
        { t, u -> Files.isRegularFile(t) && t.endsWith(recordingFilename) })
        .findAny()
        .orElseThrow({ FileNotFoundException("$recordingFilename doesn't exists in $outputDir") })
    Files.copy(recording, destPath, StandardCopyOption.REPLACE_EXISTING)
}