Wydajność Java z wykorzystaniem microbenchmark

Widzimy się już za kilka dni, 30 czerwca, aleja Józefa Piłsudskiego 31,
Rzeszów. Zaczynamy o godzinie 9.00. Zanim jednak zanurzymy się w odmęty
JVMa, narzędzi, microbenchmarków i rozważań filozoficznych nad aspektami wydajności, mam do was małą prośbę. Przygotujcie swoje laptopy do pracy, by podczas warsztatów nie tracić czasu na konfigurację i od razu rzucić się w wir pracy, bo materiału i kodu mamy trochę do przepracowania razem. Warsztat zakłada 80% czasu pracy z kodem, 20% mnie machającego rękami przy tablicy.

Niezbędne kroki przed warsztatem. Upewnij się, że masz następujące narzędzia:

* Java 16, https://adoptopenjdk.net/?variant=openjdk16&jvmVariant=hotspot
* docker, https://www.docker.com/get-started
* Java Mission Control 8, https://adoptopenjdk.net/jmc.html 
* obraz docker, `docker pull symentis/jvmperformance-workshop:latest`
* projekt z github, `git clone https://github.com/symentispl/jvmperformance-workshop`
* zbuduj projekt `mvn verify`
* sprawdź czy wszystko działa, w przypadku Linux'a `docker run -it --rm -v $(pwd):/work -v /etc/passwd:/etc/passwd -v /etc/group:/etc/group -v $HOME/.m2:$HOME/.m2 -w /work -u $(id -u ${USER}):$(id -g ${USER}) --cap-add=CAP_SYS_ADMIN  symentis/jvmperformance-workshop:latest -l`

W razie problemów z środowiskiem, skontaktuj się ze mną (jaroslaw.palka@symentis.pl). W dzień szkolenie w razie problemów z dotarciem na miejsce, będę dostępny pod telefonem, 603886892.

Do zobaczenia w środę, pamiętajcie zabrać dobry humor i otwartą głowę.

Pozdrawiam,
Jarek